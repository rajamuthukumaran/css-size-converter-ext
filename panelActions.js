//--- unit code used ---
// 0 - rem
// 1 - em
// 2 - px

//------- map events -------
document.getElementById('ext-pnl-form-css-converter').addEventListener('submit', setAsParent)

//text field
document.getElementById('txt-em').addEventListener('keyup', handleChange)
document.getElementById('txt-rem').addEventListener('keyup', handleChange)
document.getElementById('txt-px').addEventListener('keyup', handleChange)

//radio button
document.getElementById('rad-em').addEventListener('click', handleChange)
document.getElementById('rad-rem').addEventListener('click', handleChange)
document.getElementById('rad-px').addEventListener('click', handleChange)

//convertion field style
document.getElementById('rad-px').addEventListener('focusin', () => document.getElementById('txt-slid-box').classList.add('focused'))
document.getElementById('rad-px').addEventListener('focusout', () => document.getElementById('txt-slid-box').classList.remove('focused'))

document.getElementById('rad-rem').addEventListener('focusin', () => document.getElementById('txt-slid-box').classList.add('focused'))
document.getElementById('rad-rem').addEventListener('focusout', () => document.getElementById('txt-slid-box').classList.remove('focused'))

document.getElementById('rad-em').addEventListener('focusin', () => document.getElementById('txt-slid-box').classList.add('focused'))
document.getElementById('rad-em').addEventListener('focusout', () => document.getElementById('txt-slid-box').classList.remove('focused'))

document.getElementById('txt-px').addEventListener('focusin', () => document.getElementById('txt-slid-box').classList.add('focused'))
document.getElementById('txt-px').addEventListener('focusout', () => document.getElementById('txt-slid-box').classList.remove('focused'))
// --------------------------------

document.getElementById('txt-px').focus()

// get input/output element id
function getId(type, tag = "txt") {
    const idType = type === 0 ? 'rem' : type === 1 ? 'em' : type === 2 ? 'px' : type
    return `${tag}-${idType}`
}

// get all the values
function getValues() {
    const fetchValue = (id) => parseFloat(document.getElementById(id).value || 0)
    const getInpType = () => {
        let type
        document.querySelectorAll('.inp-type-rad').forEach(i => {
            if (i.checked) {
                type = i.value
                document.getElementsByClassName(i.id)[0].classList.add('selected')
                i.classList.add('selected')
            } else {
                i.classList.remove('selected')
                document.getElementsByClassName(i.id)[0].classList.remove('selected')
            }
        })
        return parseInt(type)
    }

    return {
        rootSize: fetchValue(getId(0)),
        parentSize: fetchValue(getId(1)),
        pxSize: fetchValue(getId(2)),
        inpType: getInpType()
    }
}

// output values
function outField(output) {
    const changeEle = (id, value) => {
        document.getElementById(id).innerHTML = value
    }

    output.forEach((i, index) => {
        const idType = `out${index + 1}`
        const value = i.value === Infinity || isNaN(i.value) ? '-' : i.value

        changeEle(getId(idType, 'ans'), value)
        changeEle(getId(idType, 'unit'), i.unit)
    })
}

function handleChange() {
    const values = getValues()

    if (values.inpType === 2) {
        document.querySelector('.set-btn').classList.remove('disabled')
    } else {
        document.querySelector('.set-btn').classList.add('disabled')
    }

    calculateSize(values)
}

function setAsParent(e) {
    e.preventDefault()
    if (e.target.elements?.inpType?.value == 2) {
        const value = e.target.elements?.pxSize?.value
        document.getElementById(getId(1)).value = value
        document.getElementById(getId(2)).value = ''
    }
}

function calculateSize(values) {
    // prettier-ignore
    const { rootSize, parentSize, pxSize, inpType } = values

    if (!rootSize || !parentSize) {
        let base = "You need to enter"

        if (!rootSize) base += ' HTML size to calculate rem'
        if (!rootSize && !parentSize) base += ' and'
        if (!parentSize) base += ' Parent tag size to calculate em'

        document.getElementById('err-msg').innerHTML = base
    } else {
        document.getElementById('err-msg').innerHTML = ''
    }

    // incase input px
    if (inpType === 2) {
        outField([
            { value: pxSize / parentSize, unit: 'em' },
            { value: pxSize / rootSize, unit: 'rem' }
        ])
    }
    //incase input em
    else if (inpType === 1) {
        const pxValue = pxSize * parentSize
        outField([
            { value: pxValue, unit: 'px' },
            { value: pxValue / rootSize, unit: 'rem' }
        ])
    }
    //incase input rem
    else {
        const pxValue = pxSize * rootSize
        outField([
            { value: pxValue, unit: 'px' },
            { value: pxValue / parentSize, unit: 'em' }
        ])
    }
}